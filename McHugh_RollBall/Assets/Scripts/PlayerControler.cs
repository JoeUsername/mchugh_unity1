﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControler : MonoBehaviour {

    public float speed;
    public Text CountText;

    private Rigidbody rb;
    private int count;
    
    public Text winText;
    public Text loseText;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 1;
        SetCountText();
        winText.text = "";
        loseText.text = "";
        
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Danger"))
        {
            other.gameObject.SetActive(false);
            count = count - 2;
            SetCountText();
        }
    }

    void SetCountText ()
    {
        CountText.text = "Count: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You Win!";
        }
        if (count <= 0)
        {
            loseText.text = "You Suck!";
        }
    }
}
